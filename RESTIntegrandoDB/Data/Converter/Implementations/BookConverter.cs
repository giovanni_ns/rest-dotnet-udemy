using RESTIntegrandoDB.Data.Converter.Contact;
using RESTIntegrandoDB.Data.VO;
using RESTIntegrandoDB.Model;

namespace RESTIntegrandoDB.Data.Converter.Implementations
{
    public class BookConverter : IParser<BookVO, Book>, IParser<Book, BookVO>
    {
        public Book Parse(BookVO origin)
        {
            if (origin == null) return null;
            return new Book
            {
                Id = origin.Id,
                Author = origin.Author,
                LauchDate = origin.LauchDate,
                Price = origin.Price,
                Title = origin.Title
            };
        }

        public BookVO Parse(Book origin)
        {
            {
                if (origin == null) return null;
                return new BookVO
                {
                    Id = origin.Id,
                    Author = origin.Author,
                    LauchDate = origin.LauchDate,
                    Price = origin.Price,
                    Title = origin.Title
                };
            }
        }


        public List<BookVO> Parse(List<Book> origin)
        {
            if (origin == null) return null;
            return origin.Select(item => Parse(item)).ToList();
        }

        public List<Book> Parse(List<BookVO> origin)
        {
            return origin.Select(item => Parse(item)).ToList();
        }
    }
}