using System.ComponentModel.DataAnnotations.Schema;

namespace RESTIntegrandoDB.Model.Entity
{
    public class BaseEntity
    {
        [Column("id")]
        public long Id { get; set; }
    }
}