using System.ComponentModel.DataAnnotations.Schema;
using RESTIntegrandoDB.Model.Entity;

namespace RESTIntegrandoDB.Model
{
    [Table("books")]
    public class Book : BaseEntity
    {
        [Column("author")]
        public string Author { get; set; }

        [Column("launch_date")]
        public DateTime LauchDate { get; set; }

        [Column("price")]
        public decimal Price { get; set; }

        [Column("title")]
        public string Title { get; set; }
    }
}