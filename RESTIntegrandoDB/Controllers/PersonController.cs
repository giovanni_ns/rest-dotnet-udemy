using Microsoft.AspNetCore.Mvc;
using RESTIntegrandoDB.Data.VO;
using RESTIntegrandoDB.Services;

namespace RESTIntegrandoDB.Controllers;

[ApiController]
[Route("api/[controller]")]
public class PersonController : ControllerBase
{
    private readonly ILogger<PersonController> _logger;
    private IPersonService _personService;

    public PersonController(ILogger<PersonController> logger, IPersonService personService)
    {
        _logger = logger;
        _personService = personService;
    }

    // HTTP GET request to retrieve all persons
    [HttpGet]
    public IActionResult GetAll()
    {
        // Retrieve all persons using the _personService
        return Ok(_personService.FindAll());
    }

    // HTTP GET request to retrieve a person by ID
    [HttpGet("{id}")]
    public IActionResult GetId(long id)
    {
        // Find the person by ID using the _personService
        var person = _personService.FindByID(id);

        // If the person is not found, return a 404 Not Found response
        if (person == null)
            return NotFound();

        // Return the person found with a 200 OK response
        return Ok(person);
    }

    // HTTP POST request to create a new person
    [HttpPost]
    public IActionResult Post([FromBody] PersonVO person)
    {
        // If the person object is null, return a 400 Bad Request response
        if (person == null)
            return BadRequest();

        // Create the person using the _personService and return the created person with a 200 OK response
        return Ok(_personService.Create(person));
    }

    // HTTP PUT request to update an existing person
    [HttpPut]
    public IActionResult Put([FromBody] PersonVO person)
    {
        // If the person object is null, return a 400 Bad Request response
        if (person == null)
            return BadRequest();

        // Update the person using the _personService and return the updated person with a 200 OK response
        return Ok(_personService.Update(person));
    }

    // HTTP DELETE request to delete a person by ID
    [HttpDelete("{id}")]
    public IActionResult DeletePerson(long id)
    {
        // Delete the person by ID using the _personService
        _personService.Delete(id);

        // Return a 204 No Content response to indicate successful deletion
        return NoContent();
    }

}
