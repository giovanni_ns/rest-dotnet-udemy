using Microsoft.AspNetCore.Mvc;
using RESTIntegrandoDB.Data.VO;
using RESTIntegrandoDB.Services;

namespace RESTIntegrandoDB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookController : ControllerBase
    {
        private readonly ILogger<BookController> _logger;
        private IBookService _bookService;

        public BookController(ILogger<BookController> logger, IBookService bookService)
        {
            _logger = logger;
            _bookService = bookService;
        }

        // HTTP GET request to retrieve all Books
        [HttpGet]
        public IActionResult GetAll()
        {
            // Retrieve all Books using the _BookService
            return Ok(_bookService.FindAll());
        }

        // HTTP GET request to retrieve a Book by ID
        [HttpGet("{id}")]
        public IActionResult GetId(long id)
        {
            // Find the Book by ID using the _BookService
            var Book = _bookService.FindByID(id);

            // If the Book is not found, return a 404 Not Found response
            if (Book == null)
                return NotFound();

            // Return the Book found with a 200 OK response
            return Ok(Book);
        }

        // HTTP POST request to create a new Book
        [HttpPost]
        public IActionResult Post([FromBody] BookVO book)
        {
            // If the Book object is null, return a 400 Bad Request response
            if (book == null)
                return BadRequest();

            // Create the Book using the _BookService and return the created Book with a 200 OK response
            return Ok(_bookService.Create(book));
        }

        // HTTP PUT request to update an existing Book
        [HttpPut]
        public IActionResult Put([FromBody] BookVO book)
        {
            // If the Book object is null, return a 400 Bad Request response
            if (book == null)
                return BadRequest();

            // Update the Book using the _BookService and return the updated Book with a 200 OK response
            return Ok(_bookService.Update(book));
        }

        // HTTP DELETE request to delete a Book by ID
        [HttpDelete("{id}")]
        public IActionResult DeleteBook(long id)
        {
            // Delete the Book by ID using the _BookService
            _bookService.Delete(id);

            // Return a 204 No Content response to indicate successful deletion
            return NoContent();
        }
    }
}