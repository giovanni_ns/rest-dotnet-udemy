using RESTIntegrandoDB.Data.Converter.Implementations;
using RESTIntegrandoDB.Data.VO;
using RESTIntegrandoDB.Model;
using RESTIntegrandoDB.Repository;

namespace RESTIntegrandoDB.Services.Implementations
{
    class BookServiceImplementation : IBookService
    {
        private readonly IRepository<Book> _repository;

        private readonly BookConverter _converter;

        public BookServiceImplementation(IRepository<Book> repository)
        {
            _repository = repository;
            _converter = new BookConverter();
        }

        // Create a new Book
        public BookVO Create(BookVO book)
        {
            var BookEntity = _converter.Parse(book);
            BookEntity = _repository.Create(BookEntity);
            return _converter.Parse(BookEntity);
        }

        // Delete a Book by ID
        public void Delete(long id)
        {
            _repository.Delete(id);
        }

        // Retrieve all Books
        public List<BookVO> FindAll()
        {
            return _converter.Parse(_repository.FindAll());
        }

        // Find a Book by ID
        public BookVO FindByID(long id)
        {
            return _converter.Parse(_repository.FindByID(id));
        }

        // Update a Book
        public BookVO Update(BookVO book)
        {
            var BookEntity = _converter.Parse(book);
            BookEntity = _repository.Update(BookEntity);
            return _converter.Parse(BookEntity);
        }
    }
}
