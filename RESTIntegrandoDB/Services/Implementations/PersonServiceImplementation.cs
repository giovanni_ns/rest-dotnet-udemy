using RESTIntegrandoDB.Data.Converter.Implementations;
using RESTIntegrandoDB.Data.VO;
using RESTIntegrandoDB.Model;
using RESTIntegrandoDB.Repository;

namespace RESTIntegrandoDB.Services.Implementations
{
    class PersonServiceImplementation : IPersonService
    {
        private readonly IRepository<Person> _repository;

        private readonly PersonConverter _converter;

        public PersonServiceImplementation(IRepository<Person> repository)
        {
            _repository = repository;
            _converter = new PersonConverter();
        }

        // Create a new person
        public PersonVO Create(PersonVO person)
        {
            var PersonEntity = _converter.Parse(person);
            PersonEntity = _repository.Create(PersonEntity);
            return _converter.Parse(PersonEntity);
        }

        // Delete a person by ID
        public void Delete(long id)
        {
           _repository.Delete(id);
        }

        // Retrieve all persons
        public List<PersonVO> FindAll()
        {
            return _converter.Parse(_repository.FindAll());
        }

        // Find a person by ID
        public PersonVO FindByID(long id)
        {
            return _converter.Parse(_repository.FindByID(id));
        }

        // Update a person
        public PersonVO Update(PersonVO person)
        {
           var PersonEntity = _converter.Parse(person);
            PersonEntity = _repository.Update(PersonEntity);
            return _converter.Parse(PersonEntity);
        }
    }
}
