using Microsoft.EntityFrameworkCore;
using RESTIntegrandoDB.Model.Context;
using RESTIntegrandoDB.Repository;
using RESTIntegrandoDB.Repository.Generic;
using RESTIntegrandoDB.Services;
using RESTIntegrandoDB.Services.Implementations;
using Serilog;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddCors(options => options.AddDefaultPolicy(builder =>
{
    builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader();
}));

builder.Services.AddControllers();

var connection = builder.Configuration.GetConnectionString("MySQLConnectionString");
var version = new MySqlServerVersion(builder.Configuration.GetConnectionString("MySQLConnectionVersion"));
builder.Services.AddDbContext<MySQLContext>(options => options.UseMySql(connection, version));

// Dependency injection
builder.Services.AddScoped<IPersonService, PersonServiceImplementation>();

builder.Services.AddScoped<IBookService, BookServiceImplementation>();
builder.Services.AddScoped(typeof(IRepository<>), typeof(GenericRepository<>));

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    MigrateDatabase(connection);
    app.UseSwagger();
    app.UseSwaggerUI();
}

void MigrateDatabase(string connection)
{
    try
    {
        var evolveConnection = new MySqlConnector.MySqlConnection(connection);
        var evolve = new EvolveDb.Evolve(evolveConnection, msg => Log.Information(msg))
        {
            Locations = new List<string> { "db/migrations", "db/dataset" },
            IsEraseDisabled = true,
        };
        evolve.Migrate();
    }
    catch (System.Exception ex)
    {
        Log.Error("Database migration failed", ex);
        throw;
    }
}

// app.UseHttpsRedirection();

app.UseCors();

app.UseAuthorization();

app.MapControllers();

app.Run();
