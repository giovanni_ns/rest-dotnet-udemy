INSERT INTO restudemy.person (firstname,lastname,address,gender) VALUES
	 ('Giovanni','Sadauscas','Osasco - São Paulo - Brasil','Male'),
	 ('Fernanda','Tsutsumi','São Paulo - São Paulo - Brasil','Female'),
	 ('Tobias','Bolotas','Osasco - São Paulo - Brasil','Dog'),
	 ('Cacau','Bolotas','São Paulo - São Paulo - Brasil','Dog'),
	 ('Emilio','Modenez','Ipiranga - São Paulo - Brasil','Male'),
	 ('Frodo','Modenez','Ipiranga - São Paulo - Brasil','Dog');
