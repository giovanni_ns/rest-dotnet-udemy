CREATE TABLE books (
    id INT PRIMARY KEY AUTO_INCREMENT,
    author longtext,
    launch_date datetime(6) NOT NULL,
    price decimal(65,2) NOT NULL,
    title longtext
) 